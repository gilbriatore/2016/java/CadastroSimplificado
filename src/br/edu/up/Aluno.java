package br.edu.up;

public class Aluno extends Pessoa {

	private String matricula;
	private String disciplina;
	private double nota;

	public String getDisciplina() {
		return disciplina;
	}

	public void setDisciplina(String disciplina) {
		this.disciplina = disciplina;
	}

	public double getNota() {
		return nota;
	}

	public void setNota(double nota) {
		this.nota = nota;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	
	public boolean isAprovado(){
		boolean isAprovado = false;
		if (nota >= 6){
			isAprovado = true;
		}
		return isAprovado;
	}
}
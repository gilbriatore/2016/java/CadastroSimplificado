package br.edu.up;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Programa {

	private static Scanner leitor = new Scanner(System.in);
	private static List<Aluno> listaDeAlunos = new ArrayList<>();
	private static List<Professor> listaDeProfessores = new ArrayList<>();

	public static void main(String[] args) {
		carregarDados();
		mostrarMenu();
	}

	private static void mostrarMenu() {

		String escolha = "0";

		System.out.println();
		System.out.println("---------- MENU ----------");
		System.out.println();
		System.out.println("1. Cadastrar aluno");
		System.out.println("2. Listar alunos");
		System.out.println("3. Cadastrar professor");
		System.out.println("4. Listar professores");
		System.out.println("5. Sair");
		System.out.println();
		escolha = leitor.nextLine();

		switch (escolha) {
		case "1":
			cadastrarAluno();
			break;
		case "2":
			listarAlunos();
			break;
		case "3":
			cadastrarProfessor();
			break;
		case "4":
			listarProfessores();
			break;
		case "5":
			gravarDados();
			leitor.close();
			System.out.println();
			System.out.println("Programa encerrado!");
			break;
		default:
			mostrarMenu();
			break;
		}
	}

	private static void cadastrarAluno() {

		Aluno aluno = new Aluno();

		System.out.println();
		System.out.println("--- CASDASTRO DE ALUNO ---");
		System.out.println("Informar o nome:");
		aluno.setNome(leitor.nextLine());
		System.out.println("Informar a matr�cula:");
		aluno.setMatricula(leitor.nextLine());
		System.out.println("Informar a idade:");
		aluno.setIdade(Integer.parseInt(leitor.nextLine()));
		System.out.println("Informar o sexo:");
		aluno.setSexo(leitor.nextLine());
		System.out.println("Informar a disciplina");
		aluno.setDisciplina(leitor.nextLine());
		System.out.println("Informar a nota");
		aluno.setNota(Double.parseDouble(leitor.nextLine()));
		System.out.println();

		listaDeAlunos.add(aluno);
		mostrarMenu();
	}

	private static void listarAlunos() {

		System.out.println();
		System.out.println("---- LISTA DE ALUNOS ----");
		System.out.println();
		if (listaDeAlunos.size() == 0) {
			System.out.println("Nenhum aluno cadastrado!");
		} else {
			for (Aluno aluno : listaDeAlunos) {
				System.out.println("--------------------");
				System.out.println("Aluno: " + aluno.getNome());
				System.out.println("Matr�cula: " + aluno.getMatricula());
				System.out.println("Idade: " + aluno.getIdade());
				System.out.println("Sexo: " + aluno.getSexo());
				System.out.println("Disciplina: " + aluno.getDisciplina());
				System.out.println("Nota: " + aluno.getNota());
				if (aluno.isAprovado()){
					System.out.println("Situa��o: Aprovado!");
				} else {
					System.out.println("Situa��o: Reprovado!");
				}
			}
		}
		System.out.println();
		mostrarMenu();
	}

	private static void cadastrarProfessor() {

		Professor professor = new Professor();

		System.out.println();
		System.out.println("--- CASDASTRO DE PROFESSOR ---");
		System.out.println("Informar o nome:");
		professor.setNome(leitor.nextLine());
		System.out.println("Informar a idade:");
		professor.setIdade(Integer.parseInt(leitor.nextLine()));
		System.out.println("Informar o sexo:");
		professor.setSexo(leitor.nextLine());
		System.out.println("Informar a disciplina");
		professor.setDisciplina(leitor.nextLine());
		System.out.println();

		listaDeProfessores.add(professor);
		mostrarMenu();

	}

	private static void listarProfessores() {

		System.out.println();
		System.out.println("---- LISTA DE PROFESSORES ----");
		System.out.println();
		if (listaDeProfessores.size() == 0) {
			System.out.println("Nenhum professor cadastrado!");
		} else {
			for (Professor professor : listaDeProfessores) {
				System.out.println("--------------------");
				System.out.println("Professor: " + professor.getNome());
				System.out.println("Idade: " + professor.getIdade());
				System.out.println("Sexo: " + professor.getSexo());
				System.out.println("Disciplina: " + professor.getDisciplina());
			}
		}
		System.out.println();
		mostrarMenu();
	}

	private static void carregarDados() {

		try {
			File arquivoDeAlunos = new File("arquivo_de_alunos.txt");
			Scanner leitorArquivoAlunos = new Scanner(arquivoDeAlunos);
			while (leitorArquivoAlunos.hasNextLine()) {
				String linha = leitorArquivoAlunos.nextLine();
				String[] dados = linha.split(";");
				Aluno aluno = new Aluno();
				aluno.setNome(dados[0]);
				aluno.setMatricula(dados[1]);
				aluno.setIdade(Integer.parseInt(dados[2]));
				aluno.setSexo(dados[3]);
				aluno.setDisciplina(dados[4]);
				aluno.setNota(Double.parseDouble(dados[5]));
				listaDeAlunos.add(aluno);
			}
			leitorArquivoAlunos.close();
		} catch (FileNotFoundException e) {
			// Nenhum aluno foi cadastrado ainda.
		}

		try {
			File arquivoDeProfessores = new File("arquivo_de_professores.txt");
			Scanner leitorArquivoProfessores = new Scanner(arquivoDeProfessores);
			while (leitorArquivoProfessores.hasNextLine()) {
				String linha = leitorArquivoProfessores.nextLine();
				String[] dados = linha.split(";");
				Professor professor = new Professor();
				professor.setNome(dados[0]);
				professor.setIdade(Integer.parseInt(dados[1]));
				professor.setSexo(dados[2]);
				professor.setDisciplina(dados[3]);
				listaDeProfessores.add(professor);
			}
			leitorArquivoProfessores.close();
		} catch (FileNotFoundException e) {
			// Nenhum professor foi cadastrado ainda.
		}
	}

	private static void gravarDados() {
		
		try {
			File arquivoDeAlunos = new File("arquivo_de_alunos.txt");
			FileWriter fw = new FileWriter(arquivoDeAlunos);
			BufferedWriter bw = new BufferedWriter(fw);
			for (Aluno aluno : listaDeAlunos) {
				String linha = aluno.getNome() + ";";
				linha += aluno.getMatricula() + ";";
				linha += aluno.getIdade() + ";";
				linha += aluno.getSexo() + ";";
				linha += aluno.getDisciplina() + ";";
				linha += aluno.getNota();
				bw.write(linha);
				bw.newLine();
			}
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			File arquivoDeAlunos = new File("arquivo_de_professores.txt");
			FileWriter fw = new FileWriter(arquivoDeAlunos);
			BufferedWriter bw = new BufferedWriter(fw);
			for (Professor professor : listaDeProfessores) {
				String linha = professor.getNome() + ";";
				linha += professor.getIdade() + ";";
				linha += professor.getSexo() + ";";
				linha += professor.getDisciplina();
				bw.write(linha);
				bw.newLine();
			}
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}